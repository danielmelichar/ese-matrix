#ifndef _STYLES_H_
#define _STYLES_H_ 1

#include <string>

const std::string styles = 
"body { color: black; background: white; }\n"
"span.red { color:red; }\n"
"hr.half { width: 60%; margin-left: auto; margin-right: auto; }\n"
"div.center { text-align: center; }\n"
"div.notice { border: solid thin; padding: 1em; margin: 1em 0; "
"background: #ddd; text-align: center; }"
"table { width: 90%; margin-left: auto; margin-right: auto; }\n"
"tr.title, td.title { color: white; background: black; font-weight: bold; "
"text-align: center; }\n"
"tr.data, td.data { background: #ddd; }\n"
"td.form { background: #ddd; text-align: center; }\n"
;

#endif /* _STYLES_H_ */
