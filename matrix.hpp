/* vim: set et fenc=utf-8 ff=unix sts=0 sw=4 ts=4 : */

#include <array>
#include <exception>
#include <initializer_list>

namespace mat
{

// Array used as representation for a 1D vector
template <typename T, size_t C>
using Vec = std::array<T, C>;

template <size_t R, size_t C = R, typename T = int>
class Matrix
{
    // Row, Col, Matrix definition
    using RowType = Vec<T, C>;
    using ColType = Vec<T, R>;
    using ThisType = Mat<R, C, T>;

    // Type teplmate parameters
    using TRef = typename RowType::reference;
    using TCRef = typename RowType::const_reference;
    using StorageType = std::array<RowType, R>;

    // Basic Matrix attributes
    constexpr static size_t ELEM_COUNT = R * C;
    constexpr static size_t ROW_COUNT = R;
    constexpr static size_t COL_COUNT = C;

    // deconstruction, memory free
    ~Mat<R, C, T>() = default; 

    // default constructors
    constexpr Mat<R, C, T>() = default;

    constexpr Mat<R, C, T>(const ThisType &) = default;     // actual type

    constexpr Mat<R, C, T>(ThisType &&) noexcept = default;     // reference type

    

}

} // namespace mat