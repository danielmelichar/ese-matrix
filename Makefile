CXX = g++
CFLAGS = -std=c++17 -Wall -Wextra -Werror
BUILD = build/
LIB = cgicc/lib
INCLUDE = cgicc/include
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
OUT := generate show list delete calculate
OBJS := generate.o show.o list.o delete.o calculate.o

$(info $(shell mkdir -p $(BUILD)))

all: generate show list delete calculate

$(OUT): $(OBJS)
	$(CC) -g $(BUILD)$@.o -o $(BUILD)$@.cgi -L$(ROOT_DIR)/$(LIB)

%.o: %.cpp
	$(CC) -c $(CFLAGS) -I $(INCLUDE) -o $(BUILD)$@ $<

clean:
	rm -rf $(BUILD)
